package com.outofmemo.xml.pmd.model;

/**
 * Violation POJO
 * 
 * @author Sagar Chaudhari
 *
 */
public class Violation {

	private String name;
	private String description;
	private String message;
	private String file;
	private Object lineNumber;
	private String priority;

	public Violation() {

	}

	public Violation(String name, String description, String message, String file, Object lineNumber, String priority) {
		super();
		this.name = name;
		this.description = description;
		this.message = message;
		this.file = file;
		this.lineNumber = lineNumber;
		this.priority = priority;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Object getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Object lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}
