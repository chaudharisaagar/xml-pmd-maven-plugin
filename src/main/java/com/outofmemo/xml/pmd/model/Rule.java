package com.outofmemo.xml.pmd.model;

/**
 * Rule POJO.
 * 
 * @author Sagar Chaudhari
 *
 */
public class Rule {

	private String name;
	private String description;
	private String priority;
	private String hasRoot;
	private String element;
	private String hasAttribute;
	private String withValue;
	private String matchesCase;
	private String hasParent;
	private String hasChild;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getHasRoot() {
		return hasRoot;
	}

	public void setHasRoot(String hasRoot) {
		this.hasRoot = hasRoot;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getHasAttribute() {
		return hasAttribute;
	}

	public void setHasAttribute(String hasAttribute) {
		this.hasAttribute = hasAttribute;
	}

	public String getWithValue() {
		return withValue;
	}

	public void setWithValue(String withValue) {
		this.withValue = withValue;
	}

	public String getMatchesCase() {
		return matchesCase;
	}

	public void setMatchesCase(String matchesCase) {
		this.matchesCase = matchesCase;
	}

	public String getHasParent() {
		return hasParent;
	}

	public void setHasParent(String hasParent) {
		this.hasParent = hasParent;
	}

	public String getHasChild() {
		return hasChild;
	}

	public void setHasChild(String hasChild) {
		this.hasChild = hasChild;
	}

}
