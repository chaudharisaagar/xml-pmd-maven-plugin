package com.outofmemo.xml.pmd.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * RuleSet POJO
 * 
 * @author Sagar Chaudhari
 *
 */
@JacksonXmlRootElement(localName = "ruleSet")
public class RuleSet {

	public List<Rule> rule = new ArrayList<Rule>();

}
