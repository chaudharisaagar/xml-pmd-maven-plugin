package com.outofmemo.xml.pmd;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.outofmemo.xml.pmd.model.Rule;
import com.outofmemo.xml.pmd.model.Violation;

/**
 * XML PMD Implementation
 * 
 * @author Sagar Chaudhari
 *
 */
@Mojo(name = "xml-pmd", defaultPhase = LifecyclePhase.PACKAGE)
public class XMLPMDMojo extends AbstractMojo {

	@Parameter(property = "outputDirectory", required = true)
	private File outputDirectory;

	@Parameter(property = "configDirectory", required = true)
	private File configDirectory;

	@Parameter(property = "rulesFilePath", required = true)
	private String rulesFilePath;

	private final FileSetManager fileSetManager;

	private List<Violation> violations;

	@Inject
	public XMLPMDMojo(FileSetManager fileSetManager) {
		this.fileSetManager = fileSetManager;
	}

	public void execute() throws MojoExecutionException {
		try {
			violations = new ArrayList<Violation>();

			FileSet fileset = new FileSet();
			fileset.addInclude("**/*.xml");
			fileset.setDirectory(configDirectory.getAbsolutePath());

			// If output directory doesn't exist, it will be created
			if (!outputDirectory.exists()) {
				outputDirectory.mkdirs();
			}

			// Load rules file
			List<Rule> rules = loadRules();

			// Loop through all files and validate against rule
			for (String file : fileSetManager.getIncludedFiles(fileset)) {
				validateFile(fileset.getDirectory() + File.separator + file, rules);
			}

			// Write violation results
			logViolations();

		} catch (Exception e) {
			throw new MojoExecutionException("Error occurred", e);
		}

	}

	private void logViolations() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		String jsonStr = objectMapper.writeValueAsString(violations);
		FileUtils.writeStringToFile(new File(outputDirectory.getAbsolutePath() + "/xml-pmd/results.json"), jsonStr);
	}

	private void validateFile(String file, List<Rule> rules) throws Exception {
		getLog().debug("XML PMD is validating " + file);

		File parseFile = new File(file);
		InputStream fileInputStream = new FileInputStream(parseFile);
		Document doc = PositionalXMLReader.readXML(fileInputStream);
		doc.getDocumentElement().normalize();

		for (Rule rule : rules) {
			getLog().debug("Validating rule : " + rule.getName());

			// check if root is present
			String hasRoot = rule.getHasRoot();
			if (StringUtils.isNotBlank(hasRoot)) {
				String root = doc.getDocumentElement().getNodeName();
				if (!StringUtils.equalsIgnoreCase(hasRoot, root)) {
					String message = "Expected: " + hasRoot + ", Actual: " + root;
					violations.add(new Violation(rule.getName(), rule.getDescription(), message, file,
							doc.getDocumentElement().getUserData("lineNumber"), rule.getPriority()));
				}
			}

			String element = rule.getElement();
			if (StringUtils.isNotBlank(element)) {
				NodeList nodeList = doc.getElementsByTagName(element);
				if (nodeList != null && nodeList.getLength() > 0) {

					// check if attribute is present
					String hasAttribute = rule.getHasAttribute();
					if (StringUtils.isNotBlank(hasAttribute)) {
						for (int i = 0; i < nodeList.getLength(); i++) {
							Node node = nodeList.item(i);
							if (node != null) {
								NamedNodeMap nodeAttributes = node.getAttributes();
								if (nodeAttributes != null) {
									Node nodeItem = nodeAttributes.getNamedItem(hasAttribute);
									if (nodeItem != null) {

										// check if value matches
										String withValue = rule.getWithValue();
										if (StringUtils.isNotBlank(withValue)) {
											if (!withValue.contentEquals(nodeItem.getNodeValue())) {
												String message = "Expected: " + withValue + ", Actual: "
														+ nodeItem.getNodeValue();
												violations.add(new Violation(rule.getName(), rule.getDescription(),
														message, file, node.getUserData("lineNumber"),
														rule.getPriority()));
											}
										}

										// check if matches case
										String matchesCase = rule.getMatchesCase();
										if (StringUtils.isNotBlank(matchesCase)) {
											Pattern pattern = Pattern.compile(matchesCase);
											Matcher matcher = pattern.matcher(nodeItem.getNodeValue());
											if (!matcher.find()) {
												String message = "Expected: " + matchesCase + ", Actual: Not Matching";
												violations.add(new Violation(rule.getName(), rule.getDescription(),
														message, file, node.getUserData("lineNumber"),
														rule.getPriority()));
											}
										}
									} else {
										String message = "Expected: " + hasAttribute + ", Actual: Not Found";
										violations.add(new Violation(rule.getName(), rule.getDescription(), message,
												file, node.getUserData("lineNumber"), rule.getPriority()));
									}
								}
							}
						}
					}

					// check if parent matches
					String hasParent = rule.getHasParent();
					if (StringUtils.isNotBlank(hasParent)) {
						for (int i = 0; i < nodeList.getLength(); i++) {
							Node node = nodeList.item(i);
							if (node != null) {
								Node parentNode = node.getParentNode();
								if (!hasParent.contentEquals(parentNode.getNodeName())) {
									violations.add(new Violation(rule.getName(), rule.getDescription(),
											"Expected: " + hasParent + ", Actual: Not Found", file,
											node.getUserData("lineNumber"), rule.getPriority()));
								}
							}
						}
					}

					// check if child matches
					String hasChild = rule.getHasChild();
					if (StringUtils.isNotBlank(hasChild)) {
						for (int i = 0; i < nodeList.getLength(); i++) {
							Node node = nodeList.item(i);
							if (node != null) {
								boolean found = false;
								NodeList childrenNodes = node.getChildNodes();
								if (childrenNodes != null && childrenNodes.getLength() > 0) {
									for (int j = 0; j < childrenNodes.getLength(); j++) {
										Node childNode = childrenNodes.item(j);
										if (childNode != null && hasChild.contentEquals(childNode.getNodeName())) {
											found = true;
										}
									}
								}

								if (!found) {
									violations.add(new Violation(rule.getName(), rule.getDescription(),
											"Expected: " + hasChild + ", Actual: Not Found", file,
											node.getUserData("lineNumber"), rule.getPriority()));
								}
							}
						}
					}
				}
			}
		}
	}

	private List<Rule> loadRules() throws Exception {
		if (StringUtils.startsWith(rulesFilePath, "http")) {
			BufferedInputStream stream = new BufferedInputStream(new URL(rulesFilePath).openStream());
			JacksonXmlModule xmlModule = new JacksonXmlModule();
			xmlModule.setDefaultUseWrapper(false);
			XmlMapper objectMapper = new XmlMapper(xmlModule);
			List<Rule> rules = objectMapper.readValue(stream, new TypeReference<List<Rule>>() {
			});
			stream.close();
			return rules;
		} else {
			BufferedReader reader = new BufferedReader(new FileReader(rulesFilePath));
			JacksonXmlModule xmlModule = new JacksonXmlModule();
			xmlModule.setDefaultUseWrapper(false);
			XmlMapper objectMapper = new XmlMapper(xmlModule);
			List<Rule> rules = objectMapper.readValue(reader, new TypeReference<List<Rule>>() {
			});
			reader.close();
			return rules;
		}
	}
}
