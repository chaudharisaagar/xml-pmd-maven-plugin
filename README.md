[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.outofmemo.plugins/xml-pmd-maven-plugin/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.outofmemo.plugins/xml-pmd-maven-plugin)

# XML PMD
The maven plugin is developed mainly to validate XML files against defined rules.

Plugin will validate the XML files against the rules defined in rule file and finally generate the results. The plugin will not "fail" the build instead it allows developers to analyze the results file and fix the violations if necessary.

----
## Usage
* Define rules file
* Configure maven plugin

----
## Rule File

| Field Name | Field Description |
| --- | --- |
| name | Name of the rule. |
| description | Description of the rule. |
| priority | Priority of the violation. Examples: 1,2,3 or high,medium,low |
| hasRoot | Check if the XML file has this root element. If this element is not present, then it will be marked as violation. |
| element | Check if the XML file has this elements. There will not be any violation even if this element is not present in the XML file. |
| hasAttribute | Check if the element has this attribute. If the attribute is not present, then it will be marked as violation. |
| withValue | Check if the element attribute has this value. If the value is different, then it will be marked as violation. |
| matchesCase | Check if the element attribute value matches this case i.e. regular expression. If the value does not match with the regular expression, then it will be marked as violation. |
| hasParent | Check if the element has this parent element. If the parent element is not present, then it will be marked as violation. |
| hasChild | Check if the element has this child element. If the child element is not present, then it will be marked as violation. |

----
## Configurations

| Config Name | Config Description |
| --- | --- |
| rulesFilePath | Location of the rules file. The file can be stored in the project itself, or http url can be provided so that plugin can refer the rule file stored at remote location. |
| outputDirectory | Location of the output directory. The plugin will generate new folder xml-pmd with a file results.json. This file contains the results. |
| configDirectory | Location of the configuration files. The plugin will look for XML files within this location to validate against defined rules. |

----
## Maven Plugin Configurations

```xml
<plugin>
  <groupId>com.outofmemo.plugins</groupId>
  <artifactId>xml-pmd-maven-plugin</artifactId>
  <version>1.0</version>
  <executions>
    <execution>
      <phase>package</phase>
      <goals>
        <goal>xml-pmd</goal>
      </goals>
    </execution>
  </executions>
  <configuration>
    <rulesFilePath>${basedir}/rulespmd.xml</rulesFilePath>
    <outputDirectory>${basedir}/target</outputDirectory>
    <configDirectory>${basedir}/src/main/folder</configDirectory>
  </configuration>
</plugin>
```

## Sample Rule File

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ruleSet>
  <rule name="CheckRootElement" description="Check for the root element" priority="high" hasRoot="mule" />
  <rule name="CheckAttributeCase" description="Check for the attribute case" priority="medium" element="flow" hasAttribute="name" matchesCase="^[a-zA-Z0-9]*$" />
  <rule name="CheckAttributeCase" description="Check for the attribute case" priority="medium" element="sub-flow" hasAttribute="name" matchesCase="^[a-zA-Z0-9]*$" />
  <rule name="CheckAttribute" description="Check for the attribute" priority="high" element="logger" hasAttribute="category" />
  <rule name="CheckAttributeValue" description="Check for the attribute value" priority="high" element="custom:couchbase-config" hasAttribute="bucket" withValue="somevalue" />
  <rule name="CheckParent" description="Check for the parent element" priority="low" element="http:request-connection" hasParent="http:request-config" />
  <rule name="CheckChild" description="Check for the child element" priority="low" element="http:request-config" hasChild="http:request-connection" />
</ruleSet>

```

----
## Release Notes

### 1.0
- Initial release
- All basic functionality
- Rules for checking element value, case, parent element and child element
- Read local rule file or with http URL
- Generate violation results in json format

### 1.1
- Minor housekeeping activities
